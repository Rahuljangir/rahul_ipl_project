let papaParse = require('/home/rahul/projects/IPL/node_modules/papaparse');
let fs = require('fs');
let matches = fs.readFileSync('/home/rahul/projects/IPL/src/data/matches.csv', 'utf-8');
let deliveries = fs.readFileSync('/home/rahul/projects/IPL/src/data/deliveries.csv', 'utf-8');

const matchData = papaParse.parse(matches, {
    header: true
});

const deliveriesData = papaParse.parse(deliveries, {
    header: true
});

const match = matchData["data"];
const delivery = deliveriesData["data"];

// 1. Number of matches played per year for all the years in IPL.

function matchPlayedPerYear(match) {
    if (Array.isArray(match)) {
        let seasonWise = match.reduce(function (acc, curr) {
            if (acc[curr.season])
                acc[curr.season] += 1;
            else
                acc[curr.season] = 1;
            return acc;
        }, {});
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/matchPlayedPerYear.json", JSON.stringify(seasonWise),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("File written successfully in matchPlayedPerYear.json\n");
                }
            });
    }
    else {
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/matchPlayedPerYear.json", JSON.stringify({}),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("Empty object written successfully in matchPlayedPerYear.json\n");
                }
            });
    }
}
matchPlayedPerYear(match);

// 2. Number of matches won per team per year in IPL.

function matchesWonPerTeamPerYear(match) {
    if (Array.isArray(match)) {
        let teamWiseWinner = match.reduce((acc, curr) => {
            if (acc[curr.winner]) {
                if (acc[curr.winner][curr.season]) {
                    acc[curr.winner][curr.season] += 1;
                }
                else {
                    acc[curr.winner][curr.season] = 1;
                }
            }
            else {
                acc[curr.winner] = {};
                acc[curr.winner][curr.season] = 1;

            }
            return acc;


        }, {})
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/matchesWonPerTeamPerYear.json", JSON.stringify(teamWiseWinner),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("File written successfully in matchesWonPerTeamPerYear.json\n");
                }
            });
    }
    else {
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/matchesWonPerTeamPerYear.json", JSON.stringify({}),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("Empty object written successfully in matchesWonPerTeamPerYear.json\n");
                }
            });
    }
}
matchesWonPerTeamPerYear(match);

// 3. Extra runs conceded per team in the year 2016

function deliveriesInSingleYear(matches, deliveries, year) {
    let Id = matches.filter(match => match.season == year).map(match => parseInt(match.id));

    return deliveries.filter(delivery => Id.indexOf(parseInt(delivery.match_id)) !== -1)
}

function extraRunIn2016TeamWise(matches, deliveries, year) {
    if (Array.isArray(matches) && Array.isArray(deliveries) && year >= 2008 && year <= 2017) {
        let extraRun = deliveriesInSingleYear(matches, deliveries, year).reduce((acc, curr) => {
            if (acc[curr.bowling_team]) {
                acc[curr.bowling_team] += parseInt(curr.extra_runs);
            }
            else {
                acc[curr.bowling_team] = parseInt(curr.extra_runs);
            }
            return acc;
        }, {});
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/extraRunIn2016TeamWise.json", JSON.stringify(extraRun),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("File written successfully in extraRunIn2016TeamWise.json\n");
                }
            });
    }
    else {
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/extraRunIn2016TeamWise.json", JSON.stringify({}),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("Empty object written successfully in extraRunIn2016TeamWise.json\n");
                }
            });
    }
}
extraRunIn2016TeamWise(match, delivery, 2016);

// 4. Top 10 economical bowlers in the year 2015

function top10EconomyBowlerIn2015(matches, deliveries, year) {
    if (Array.isArray(matches) && Array.isArray(deliveries) && year >= 2008 && year <= 2017) {
        let bowlerEconomyObj = deliveriesInSingleYear(matches, deliveries, year).reduce((acc, curr) => {
            if (acc[curr.bowler]) {
                if (acc[curr.bowler]["totalRuns"]) {
                    acc[curr.bowler]["totalRuns"] += parseInt(curr.total_runs) - parseInt(curr.bye_runs) - parseInt(curr.legbye_runs);
                }
                else {
                    acc[curr.bowler]["totalRuns"] = parseInt(curr.total_runs) - parseInt(curr.bye_runs) - parseInt(curr.legbye_runs);
                }
            }
            else {
                acc[curr.bowler] = {};
                acc[curr.bowler]["totalRuns"] = parseInt(curr.total_runs) - parseInt(curr.bye_runs) - parseInt(curr.legbye_runs);
            }
            if (acc[curr.bowler]) {
                if (acc[curr.bowler]["totalBall"]) {
                    acc[curr.bowler]["totalBall"] += parseInt(curr.ball) - parseInt(curr.noball_runs != 0 ? 1 : 0) - parseInt(curr.wide_runs != 0 ? 1 : 0);
                }
                else {
                    acc[curr.bowler]["totalBall"] = parseInt(curr.ball) - parseInt(curr.noball_runs != 0 ? 1 : 0) - parseInt(curr.wide_runs != 0 ? 1 : 0);
                }
            }
            else {
                acc[curr.bowler] = {};
                acc[curr.bowler]["totalBall"] = parseInt(curr.ball) - parseInt(curr.noball_runs != 0 ? 1 : 0) - parseInt(curr.wide_runs != 0 ? 1 : 0);
            }
            acc[curr.bowler]["economy"] = (acc[curr.bowler]["totalRuns"] * 6) / acc[curr.bowler]["totalBall"];

            return acc;
        }, {});
        let economy = Object.fromEntries(Object.entries(bowlerEconomyObj)
            .sort((bowler1, bowler2) => {
                return bowler1[1].economy - bowler2[1].economy
            }).slice(0, 10));
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/top10EconomyBowlerIn2015.json", JSON.stringify(economy),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("File written successfully in top10EconomyBowlerIn2015.json\n");
                }
            });
    }
    else {
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/top10EconomyBowlerIn2015.json", JSON.stringify({}),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("Empty object written successfully in top10EconomyBowlerIn2015.json\n");
                }
            });
    }
}
top10EconomyBowlerIn2015(match, delivery, 2015);

// 1. Find the number of times each team won the toss and also won the match

function teamWonTossAndMatch(matches) {
    if (Array.isArray(matches)) {
        let WonTossAndMatch = matches.reduce((acc, curr) => {
            if (!curr.id == '') {
                if (acc[curr.toss_winner] == acc[curr.winner]) {
                    if (acc[curr.toss_winner]) {
                        acc[curr.toss_winner] += 1;
                    }
                    else {
                        acc[curr.toss_winner] = 1;
                    }
                }
            }
            return acc;
        }, {})
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/teamWonTossAndMatch.json", JSON.stringify(WonTossAndMatch),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("File written successfully in teamWonTossAndMatch.json\n");
                }
            });
    }
    else {
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/teamWonTossAndMatch.json", JSON.stringify({}),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("Empty object written successfully in teamWonTossAndMatch.json\n");
                }
            });
    }
}

console.log(teamWonTossAndMatch(match));

// 2. Find a player who has won the highest number of Player of the Match awards for each season

function playerOfMatchSeasonWise(matches) {
    if (Array.isArray(matches)) {
        let playerWon = matches.reduce((acc, curr) => {
            if (!curr.id == '') {
                if (acc[curr.season]) {
                    if (acc[curr.season][curr.player_of_match]) {
                        acc[curr.season][curr.player_of_match] += 1;
                    }
                    else {
                        acc[curr.season][curr.player_of_match] = 1;
                    }
                }
                else {
                    acc[curr.season] = {};
                    acc[curr.season][curr.player_of_match] = 1;
                }
            }
            return acc;
        }, {})
        let arrayPlayerWon = Object.entries(playerWon)
        let finalResultOfPlayerOfMatch = arrayPlayerWon.reduce((acc, curr) => {
            let sortedarray = Object.entries(curr[1])
                .sort((player1, Player2) => {
                    return Player2[1] - player1[1]
                }).slice(0, 1);

            acc[curr[0]] = Object.fromEntries(sortedarray);
            return acc;
        }, {})
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/playerOfMatchSeasonWise.json", JSON.stringify(finalResultOfPlayerOfMatch),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("File written successfully in playerOfMatchSeasonWise.json\n");
                }
            });
    }
    else {
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/playerOfMatchSeasonWise.json", JSON.stringify({}),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("Empty object written successfully in playerOfMatchSeasonWise.json\n");
                }
            });
    }
}
playerOfMatchSeasonWise(match);

// 3. Find the strike rate of a batsman for each season.


function strikeRateEachYear(matches, deliveries, year, playerName) {
    if (Array.isArray(matches) && Array.isArray(deliveries) && playerName != "") {
        let strikeRate = deliveriesInSingleYear(matches, deliveries, year).reduce((acc, curr) => {
            if (curr.batsman === playerName) {
                if (acc[curr.batsman]) {
                    acc[curr.batsman]["runs"] += parseInt(curr.batsman_runs);
                    acc[curr.batsman]["balls"] += 1;
                    acc[curr.batsman]["year"] = year;
                }
                else {
                    acc[curr.batsman] = {};
                    acc[curr.batsman]["runs"] = parseInt(curr.batsman_runs);
                    acc[curr.batsman]["balls"] = 1;
                }
                acc[curr.batsman]["strikeRate"] = parseFloat((parseInt(acc[curr.batsman]["runs"]) * 100 / parseInt(acc[curr.batsman]["balls"])).toFixed(2));

            }
            return acc;
        }, {});
        return strikeRate;

    }
}
let season = match.map((curr) => {
    return parseInt(curr.season);
});
let years = season.filter((curr, index) => {
    return season.indexOf(curr) === index;
});

let playerName = "V Kohli";
let result = years.map((year) => {
    return (strikeRateEachYear(match, delivery, year, playerName))
});

result;
fs.writeFile("/home/rahul/projects/IPL/src/public/output/strikeRateEachYear.json", JSON.stringify(result),
    (err) => {
        if (err)
            console.log(err);
        else {
            console.log("File written successfully in strikeRateEachYear.json\n");
        }
    });

// 4. Find the highest number of times one player has been dismissed by another player


function dismissalsPlayersDetails(deliveries) {
    if (Array.isArray(deliveries)) {

        let dismissedPlayerDetail = { dismissedPlayer: null, dismissedBy: null, dismissalsTime: 0 };

        let dismissedPlayers = deliveries.filter((ball) => {
            if (ball.dismissal_kind !== '' && ball.dismissal_kind !== 'run out') {
                return true;
            }
        })
        let dismissedPlayerByBowler = dismissedPlayers.reduce((acc, curr) => {

            if (acc[curr.batsman] === undefined) {
                acc[curr.batsman] = {}
            }

            if (acc[curr.batsman][curr.bowler] === undefined) {
                acc[curr.batsman][curr.bowler] = 0;
            }

            acc[curr.batsman][curr.bowler]++;

            if (dismissedPlayerDetail.dismissalsTime < acc[curr.batsman][curr.bowler]) {
                dismissedPlayerDetail.dismissedPlayer = curr.batsman;
                dismissedPlayerDetail.dismissedBy = curr.bowler;
                dismissedPlayerDetail.dismissalsTime = acc[curr.batsman][curr.bowler];
            }

            return acc;

        }, {});
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/dismissedPlayerDetail.json", JSON.stringify(dismissedPlayerDetail),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("File written successfully in dismissedPlayerDetail.json\n");
                }
            });
    }
    else {
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/dismissedPlayerDetail.json", JSON.stringify({}),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("Empty object written successfully in dismissedPlayerDetail.json\n");
                }
            });
    }
}
dismissalsPlayersDetails(delivery);


// 5. Find the bowler with the best economy in super overs.

const bestBowlerInSuperover = function (deliveries) {
    if (Array.isArray(deliveries)) {
        let bowlerInSuperOver = deliveries.reduce((acc, curr) => {
            if (curr.match_id != '') {
                if (curr.is_super_over != 0) {
                    if (acc[curr.bowler]) {
                        acc[curr.bowler]['total_runs'] += parseInt(curr.total_runs) - (parseInt(curr.bye_runs) + parseInt(curr.legbye_runs));
                        if (curr['noball_runs'] !== 0 && curr['wide_runs'] !== 0) {
                            acc[curr.bowler]['ball']++
                        }
                    } else {
                        acc[curr.bowler] = {};
                        acc[curr.bowler]['total_runs'] = parseInt(curr.total_runs) - (parseInt(curr.bye_runs) + parseInt(curr.legbye_runs));
                        if (curr['noball_runs'] !== 0 && curr['wide_runs'] !== 0) {
                            acc[curr.bowler]['ball'] = 1
                        }
                    }
                    acc[curr.bowler]['economy'] = parseFloat((acc[curr.bowler]['total_runs'] * 6) / (acc[curr.bowler]['ball']).toFixed(2));
                }
            }
            return acc;
        }, {});

        let bestEconomyBowlerInSuperOver = Object.fromEntries(Object.entries(bowlerInSuperOver)
            .sort((player1, player2) => {
                return player2[1].economy > player1[1].economy ? -1 : 1;
            }).slice(0, 1));

        fs.writeFile("/home/rahul/projects/IPL/src/public/output/bestEconomyBowlerInSuperOver.json", JSON.stringify(bestEconomyBowlerInSuperOver),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("File written successfully in bestEconomyBowlerInSuperOver.json\n");
                }
            });
    }
    else {
        fs.writeFile("/home/rahul/projects/IPL/src/public/output/bestEconomyBowlerInSuperOver.json", JSON.stringify({}),
            (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("Empty object written successfully in bestEconomyBowlerInSuperOver.json\n");
                }
            });
    }
}

console.log(bestBowlerInSuperover(delivery));